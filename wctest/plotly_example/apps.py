from django.apps import AppConfig


class PlotlyExampleConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'plotly_example'
