# DjangoWebComponentSample

## RUN

```
cd wctest
python3.10 -m venv venv
source venv/bin/activate.fish
pip install -r requirements.txt

./manage.py djwc && ./manage.py runserver
```

## References

* Python
* Venv
* Django
* [djwc](https://pypi.org/project/djwc/)
* [Django plotly dash](https://django-plotly-dash.readthedocs.io/en/latest/)